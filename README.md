# Plan

WARNING: This tool is work in progress!  Some of the features mentioned below have not
been implemented yet!

This is a program to help manage .plan files.  It does this by:

- Creating or opening .plan files in your editor.
- Managing the directories (they go in `%HOME%/.plan`, `~/.plan` or the directory defined by `PLAN_DIR` environemnt variable).
- Searching for a keyword.

## Prequisites

If you want to build from scratch, you need Rust installed.  Plan also uses
the `EDITOR` environment variable to choose your editor.

## Installation

If you have rust installed, run

```
cargo install plan
```

# Using Plan

## Creating or opening a plan

Creation and opening (in your editor given by `EDITOR` environment variable) are achieved by the same commands:

```
plan yesterday
plan today
plan tomorrow
plan date YYYY-MM-DD
```

Given a year ('YYYY'), a month ('MM') and a day ('DD'), a file will be created with this file name:
```
$PLAN_DIR/YYYY/MM/YYYY-MM-DD.plan
```

and then opened in your editor.

On saving and closing your editor, it will be indexed.

## Search

```
plan search <keyword>
```

This causes Plan to call `rg` (ripgrep) to search all your .plan files for a particular keyword.
