#![allow(unused)]

use std::{
    env,
    fs::{create_dir_all, metadata, File},
    io::{BufWriter, Write},
    path::{Path, PathBuf},
};

use chrono::{DateTime, Datelike, Days, Duration, Local, Weekday};
use clap::{Parser, Subcommand};
use color_eyre::{eyre::eyre, Report};

#[derive(Debug, Subcommand)]
enum Command {
    /// See the plan for today.
    Today,

    /// See the plan for yesterday.
    Yesterday(DayDetails),

    /// See the plan for tomorrow.
    Tomorrow(DayDetails),

    /// Create a yearly plan.
    Year(YearDetails),
}

#[derive(Debug, Parser)]
struct DayDetails {
    /// Do not ignore weekends - tomorrow from Friday is Saturday.
    #[arg(short, long)]
    weekend: bool,
}

#[derive(Debug, Parser)]
struct YearDetails {
    /// Name of the text file (without extension) to be stored.
    file_name: PathBuf,
}

#[derive(Debug, Parser)]
struct Config {
    /// Plan command
    #[command(subcommand)]
    command: Command,
}

fn main() -> color_eyre::Result<()> {
    color_eyre::install()?;

    // Obtain the environment variables we care about
    let editor = env::var("EDITOR").unwrap_or_else(|_| "vim".to_string());
    let plan_dir = env::var("PLAN_DIR").map(PathBuf::from).or_else(|_| {
        let home_dir = dirs::home_dir().ok_or(eyre!("No home directory found!"));
        home_dir.map(|home| home.join(".plans"))
    })?;

    let config = Config::parse();

    match config.command {
        Command::Today => process_plan(Local::now(), editor, plan_dir)?,
        Command::Yesterday(details) => {
            process_plan(yesterday(Local::now(), details.weekend), editor, plan_dir)?
        }
        Command::Tomorrow(details) => {
            process_plan(tomorrow(Local::now(), details.weekend), editor, plan_dir)?
        }
        Command::Year(details) => {
            generate_year_plan(Local::now().year(), details.file_name, editor, plan_dir)?
        }
    }

    Ok(())
}

/// Figure out what yesterday means possibly ignoring weekends
fn yesterday(date: DateTime<Local>, weekend: bool) -> DateTime<Local> {
    let offset = match date.weekday() {
        Weekday::Mon => {
            if weekend {
                1
            } else {
                3
            }
        }
        _ => 1,
    };

    Local::now() - Duration::days(offset)
}

/// Figure out what tomorrow means possibly ignoring weekends
fn tomorrow(date: DateTime<Local>, weekend: bool) -> DateTime<Local> {
    let offset = match date.weekday() {
        Weekday::Fri => {
            if weekend {
                1
            } else {
                3
            }
        }
        _ => 1,
    };

    Local::now() + Duration::days(offset)
}

fn open_or_generate_file(
    full_file_path: PathBuf,
    editor: String,
    generate_fn: impl FnOnce(File) -> Result<(), Report>,
) -> Result<(), Report> {
    if full_file_path.exists() {
        if !full_file_path.is_file() {
            // This file cannot be opened as it's not a file!
            return Err(eyre!("Path is not a file: {full_file_path:?}"))?;
        }
    } else {
        let file = File::create(&full_file_path)?;
        generate_fn(file)?;
    }

    let mut editor = std::process::Command::new(editor)
        .arg(full_file_path)
        .spawn()?;
    editor.wait()?;
    Ok(())
}

fn generate_year_plan(
    year: i32,
    file_name: PathBuf,
    editor: String,
    home_path: PathBuf,
) -> Result<(), Report> {
    let file_path = get_year_file_path(year, &home_path);
    ensure_path(&file_path)?;
    let full_file_path = file_path.join(file_name.with_extension("year"));

    open_or_generate_file(full_file_path, editor, |mut file| {
        // Find the date of the first week of the year
        let mut date = Local::now().with_day(1).unwrap().with_month(1).unwrap();
        let year = date.year();
        while date.weekday() != Weekday::Mon {
            date += Duration::days(1);
        }

        const MONTHS: [&str; 12] = [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        ];

        let title = format!("# Year plan for {:?}\n\n", file_name);
        file.write_all(title.as_bytes())?;
        let mut week = 1;
        while year == date.year() {
            let date_string = format!("{} {:02}", MONTHS[(date.month() - 1) as usize], date.day());
            let line = format!("{:>2} ({date_string}) - [ ] \n", week);
            file.write_all(line.as_bytes());
            date += Duration::weeks(1);
            week += 1;
        }
        Ok(())
    })?;

    Ok(())
}

fn process_plan(date: DateTime<Local>, editor: String, home_path: PathBuf) -> Result<(), Report> {
    let file_name = date.format("%Y-%m-%d.plan").to_string();
    let file_path = get_file_path(&date, &home_path);
    ensure_path(&file_path)?;
    let full_file_path = file_path.join(file_name);

    open_or_generate_file(full_file_path, editor, |mut file| {
        // File does not exist so let's create a new one
        let day = format!(
            "{}{}",
            date.day(),
            match date.day() {
                1 => "st",
                2 => "nd",
                3 => "rd",
                _ => "th",
            }
        );
        const MONTHS: [&str; 12] = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ];
        const DAYS: [&str; 7] = [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
        ];
        let month = MONTHS[(date.month() - 1) as usize].to_string();
        let year = date.format("%Y").to_string();
        let day_of_week = DAYS[date.weekday() as usize];
        let title = format!("# Plan: {day_of_week}, {day} {month} {year}\n\n\n");

        file.write_all(title.as_bytes())?;
        Ok(())
    })?;
    Ok(())
}

fn get_file_path(date: &DateTime<Local>, home: &Path) -> PathBuf {
    let year_string = date.format("%Y").to_string();
    let month_string = date.format("%m").to_string();

    home.join(year_string).join(month_string)
}

fn get_year_file_path(year: i32, home: &Path) -> PathBuf {
    let year_string = year.to_string();

    home.join(year_string)
}

fn ensure_path(path: &Path) -> Result<(), Report> {
    Ok(create_dir_all(path)?)
}
